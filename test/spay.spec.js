var sinon = require("sinon");
var chai = require('chai')
var expect = chai.expect;
var student = require("./../controllers/studentCtrl")
var studentobj = new student();

describe(" using spy check return function",function(){

        it("check userid",function(){
            expect(studentobj.userId()).to.be.equal(12);
        })

        // it("function count",function(){
        //     spyobj= sinon.spy(studentobj,'getInfo')
        //     studentobj.home(5);
        //     expect(spyobj.calledTwice).to.be.true;
        // })

        it("function arguments",function(){
            spyobj= sinon.spy(studentobj,'getInfo')
            studentobj.home(5);
            expect(spyobj.calledWith(5,1)).to.be.true;
        })
      
})


