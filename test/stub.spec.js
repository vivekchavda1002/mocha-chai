
var sinon = require("sinon");
var chai = require('chai')
var expect = chai.expect;
var student = require("./../controllers/studentCtrl")
var studentobj = new student();

describe('stub',function(){
    it("function arg",function(){
        var stub=sinon.stub(studentobj,'getexternal')
        stub.withArgs(40).returns(5)
        expect(studentobj.getfinalmarks(40)).to.be.equal(54)
    })
})